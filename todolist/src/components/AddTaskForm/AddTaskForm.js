import React from 'react';
import './AddTaskForm.css';

const AddTask = props => {
    return <div className="header">
        <input type="text" value={props.value} onChange={props.changeHandler} className="input-text"/>
        <button onClick={props.addHandler} className="add-btn">Add</button>
    </div>
};

export default AddTask;