import React from 'react';
import './Task.css';

const Tasks = props => {
    return <div className="list">
        <p>{props.name}</p>
        <button onClick={props.removeHandler} className="delete-btn">Delete</button>
    </div>
};

export default Tasks;