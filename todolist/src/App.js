import React, { Component } from 'react';
import Tasks from './components/Task/Task';
import AddForm from './components/AddTaskForm/AddTaskForm';
import './App.css';

class App extends Component {
 state = {
   newTask: '',
   tasks: [
       {name: 'Buy milk', id: 1},
       {name: 'Buy potato', id: 2},
       {name: 'Buy juice', id: 3}
   ],
     currentTask: 3
 };

 changeHandler = event => {
   this.setState({newTask: event.target.value});
 };

 addTask = () => {
  if (this.state.newTask !== '') {
    const tasks = [...this.state.tasks];
    const newTask = {name: this.state.newTask, id: this.state.currentTask + 1};
    tasks.push(newTask);

    this.setState({tasks: tasks, currentTask: this.state.currentTask + 1, newTask: ''});
  }
 };

 deleteTask = id => {
  const tasks = [...this.state.tasks];
  const index = tasks.findIndex(task => {
    return (
        task.id === id
    )
  });
  tasks.splice(index, 1);

  this.setState({tasks: tasks, currentTask: this.state.currentTask - 1});
 };

  render() {
    let list  = this.state.tasks.map((task) => {
      return (
          <div className="list-block">
              <Tasks
                  key = {task.id}
                  name = {task.name}
                  removeHandler = {() => this.deleteTask(task.id)}
              >
              </Tasks>
          </div>
      )
    });

    return (
      <div className="App">
        <AddForm
            addHandler={() => this.addTask()}
            changeHandler={event => this.changeHandler(event)}
            value={this.state.newTask}
        />
          {list}
      </div>
    );
  }
}

export default App;
